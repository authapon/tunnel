FROM archlinux

WORKDIR /root
COPY passwd.txt /root
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm openssh 
RUN ssh-keygen -A
RUN useradd -m tunnel
RUN passwd tunnel < passwd.txt
RUN passwd < passwd.txt

CMD ["/usr/bin/sshd", "-D"]
